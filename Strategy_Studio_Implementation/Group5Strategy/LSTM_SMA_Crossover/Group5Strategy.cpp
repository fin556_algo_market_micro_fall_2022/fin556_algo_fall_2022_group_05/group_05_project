#ifdef _WIN32
    #include "stdafx.h"
#endif

#include "Group5Strategy.h"

const std::string path_to_model = "/home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/Group5Strategy/lstm_model.pt";

Group5Strategy::Group5Strategy(StrategyID strategyID,
                    const std::string& strategyName,
                    const std::string& groupName):
    Strategy(strategyID, strategyName, groupName) {

	// for MSA
        small_sum = 0.0;
        big_sum = 0.0;
        small_window = 40;
        big_window = 100;
        stock_size = 0;
        revenue = 0.0;
	capital = 0.0;
	trade_times = 0;
	


	// for LSTM
	try {
		// Deserialize the ScriptModule from a file using torch::jit::load().
		model = torch::jit::load(path_to_model);
	}
	catch (const c10::Error& e) {
		std::cerr << "error loading the model\n";
	}


}

Group5Strategy::~Group5Strategy() {
}

void Group5Strategy::DefineStrategyParams() {
}

void Group5Strategy::RegisterForStrategyEvents(
    StrategyEventRegister* eventRegister, DateType currDate) {
}

void Group5Strategy::OnTrade(const TradeDataEventMsg& msg) {


    const Instrument* instrument = &msg.instrument();
    double mid_price = (msg.instrument().top_quote().ask() + msg.instrument().top_quote().bid()) / 2;


    // for LSTM
//    std::vector<torch::jit::IValue> inputs;

    std::vector<float> vector_inputs;
    float input_value = (float) mid_price;
    vector_inputs.push_back(input_value);
    auto one_dim_torch = torch::from_blob(vector_inputs.data(), vector_inputs.size(), torch::kF32);
    auto three_dim_torch = one_dim_torch.view({one_dim_torch.sizes()[0], 1, 1});
    std::vector<torch::jit::IValue> torch_input;
    torch_input.push_back(three_dim_torch);
    at::Tensor torch_output = model.forward(torch_input).toTensor();

    float predicted_price = torch_output.index({0}).item<float>();
//    float predicted_price = torch_output[1].item<float>();
//    float predicted_price = torch_input[1];
    std::cout << "predicted price = " << predicted_price << std::endl;






    small_sum += predicted_price;
    big_sum += predicted_price;

    small_set.push(predicted_price);
    big_set.push(predicted_price);

    if (small_set.size() > small_window) {
        small_sum -= small_set.front();
        small_set.pop();
    }
    if (big_set.size() > big_window) {
        big_sum -= big_set.front();
        big_set.pop();
    }

    double small_average = small_sum / (double) small_set.size();
    double big_average = big_sum / (double) big_set.size();
    
    if (small_average > big_average) { // buy
        SendOrder(instrument, 100);
        stock_size += 100;
        revenue -= instrument->top_quote().ask() * 100;
        trade_times++;
    } else if (small_average < big_average && stock_size > 0) { // sell
        SendOrder(instrument, -100);
        stock_size -= 100;
        revenue += instrument->top_quote().bid() * 100;
        trade_times++;
    } else { // do nothing

    }

    capital = stock_size * instrument->top_quote().bid() + revenue; // money in stock + money in cash

    std::cout << "capital = " << capital << " , size = " << stock_size << ", trade times = " << trade_times << std::endl;

}


void Group5Strategy::OnOrderUpdate(const OrderUpdateEventMsg& msg) {
    
    // std::cout << "name = " << msg.name() << std::endl;
    // std::cout << "order id = " << msg.order_id() << std::endl;
    // std::cout << "fill occurred = " << msg.fill_occurred() << std::endl;
    // std::cout << "update type = " << msg.update_type() << std::endl;
    
    std::cout << "time " << msg.update_time() << std::endl;

}

void Group5Strategy::OnBar(const BarEventMsg& msg) {
}

void Group5Strategy::AdjustPortfolio() {
}

void Group5Strategy::SendOrder(const Instrument* instrument, int trade_size) {

    double price;
        if (trade_size > 0) { // buy
                price = instrument->top_quote().ask();
        } else { // sell
                price = instrument->top_quote().bid();
        }

        OrderParams params(
                        *instrument,
                        abs(trade_size),
                        price,
                        MARKET_CENTER_ID_IEX,
                        (trade_size > 0) ? ORDER_SIDE_BUY : ORDER_SIDE_SELL,
                        ORDER_TIF_DAY,
                        ORDER_TYPE_LIMIT
                        );

        std::string action;
        if (trade_size > 0) {
                action = "buy ";
        } else {
                action = "sell ";
        }

        std::cout << "SendTradeOrder(): about to send new order for size "
                << trade_size
                << " at $"
                << price
                << " to "
                << action
                << instrument->symbol()
                << std::endl;

        TradeActionResult tra = trade_actions()->SendNewOrder(params);
        if (tra == TRADE_ACTION_RESULT_SUCCESSFUL) {
                std::cout << "Sending new trade order successful!" << std::endl;
        } else {
                std::cout << "Error sending new trade order..." << tra << std::endl;
        }
    
}

void Group5Strategy::OnResetStrategyState() {
}

void Group5Strategy::OnParamChanged(StrategyParam& param) {
}
