import torch
import torch.nn as nn
from torch.autograd import Variable 
import numpy as np
import time

import os
import pandas as pd

from sklearn.preprocessing import StandardScaler, MinMaxScaler
import math 

import csv

input_dim = 1
hidden_dim = 32
num_layers = 3
output_dim = 1
#num_epochs = 200
num_epochs = 20

class LSTM(nn.Module):
    def __init__(self, input_dim, hidden_dim, num_layers, output_dim):
        super(LSTM, self).__init__()
        self.hidden_dim = hidden_dim
        self.num_layers = num_layers
        
        self.lstm = nn.LSTM(input_dim, hidden_dim, num_layers, batch_first=True)
        self.fc = nn.Linear(hidden_dim, output_dim)
    def forward(self, x):
        h0 = torch.zeros(self.num_layers, x.size(0), self.hidden_dim).requires_grad_()
        print(h0.shape)
        c0 = torch.zeros(self.num_layers, x.size(0), self.hidden_dim).requires_grad_()
        out, (hn, cn) = self.lstm(x, (h0.detach(), c0.detach()))
        
        out = self.fc(out[:, -1, :]) 
        return out


def main():

    df = pd.read_csv('20221128_trades.csv') ## reading csv datasets as panda dataframe
    df['COLLECTION_TIME'] = pd.to_datetime(df['COLLECTION_TIME']) # coverting collection time to date time type
    df = df.set_index('COLLECTION_TIME')#setting collection_time as index
    df.head()

    X = df.loc[:,('PRICE')]
    y = df.loc[:,('PRICE')]


    def scaling(X):
        min_scaler = MinMaxScaler()
        X_ss = min_scaler.fit_transform(X.values.reshape(-1,1)) # X_ss is scaled version of PRICE
        return(X_ss)

    def split_dataset(stock, lookback):
        data_raw = stock# convert to numpy array
        data = []
    
        # create all possible sequences of length seq_len
        for index in range(len(data_raw) - lookback): 
            data.append(data_raw[index: index + lookback])
    
        data = np.array(data);
        #print(data.shape)
        #checking the len size
        test_len = math.ceil(len(data)*0.2)
        train_len = len(data) - (test_len);
    
        x_train = data[:train_len,:-1,:]
        y_train = data[:train_len,-1,:]
    
        x_test = data[train_len:,:-1]
        y_test = data[train_len:,-1,:]
    
        return [x_train, y_train, x_test, y_test]

    lookback = 60  

    #calling the functions in this code

    X_ss = scaling(X)#calling scaling function to normalised the price array
    x_train, y_train, x_test, y_test = split_dataset(X_ss, lookback)##calling the function 

    print(x_train.shape)
    print(y_train.shape)
    print(x_test.shape)
    print(y_test.shape)
    

    x_train_final = torch.from_numpy(x_train).type(torch.Tensor)
    x_test_final= torch.from_numpy(x_test).type(torch.Tensor)
    y_train_final = torch.from_numpy(y_train).type(torch.Tensor)
    y_test_final = torch.from_numpy(y_test).type(torch.Tensor)




    model = LSTM(input_dim, hidden_dim, num_layers, output_dim)
    # sm = torch.jit.script(model)


    hist = np.zeros(num_epochs) #creating a varibale

    lstm = []
    criterion = torch.nn.MSELoss(reduction='mean')# criteria for error
    optimiser = torch.optim.Adam(model.parameters(), lr=0.001)# optimzer for tuning the model

    for t in range(num_epochs):
        y_train_pred = model(x_train_final)# passing X_train_final dataset into LSTM model(part2)
        loss = criterion(y_train_pred, y_train_final)#
        if t%10==0:
            print("Epoch: %d, loss: %1.5f" % (t, loss.item())) # printing the loss

        hist[t] = loss.item()# saving the loss
        optimiser.zero_grad()# 
        loss.backward()# backward propogation 
        optimiser.step()#

    
    sm = torch.jit.script(model)
    sm.save("my_module_model.pt")


    print("Hello World!")

if __name__ == "__main__":
    main()